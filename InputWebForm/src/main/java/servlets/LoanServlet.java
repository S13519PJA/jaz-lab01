
package servlets;

import com.sun.org.apache.xerces.internal.impl.dv.xs.DecimalDV;
import java.io.IOException;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/loan")
public class LoanServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String koszt = request.getParameter("koszt");
        if (koszt == null || koszt.equals("") || koszt.contains("[a-zA-Z]+")) {
            response.sendRedirect("/");
        }
        String raty = request.getParameter("raty");
        if (raty == null || raty.equals("") || raty.contains("[a-zA-Z]+")) {
            response.sendRedirect("/");
        }
        String procent = request.getParameter("procent");
        if (procent == null || procent.equals("") || procent.contains("[a-zA-Z]+")) {
            response.sendRedirect("/");
        }
        String oplataStala = request.getParameter("oplataStala");
        if (oplataStala == null || oplataStala.equals("") || oplataStala.contains("[a-zA-Z]+")) {
            response.sendRedirect("/");
        }
        String oplata = request.getParameter("oplata");
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        Integer koszt = Integer.parseInt(request.getParameter("koszt"));
        if (koszt == null || koszt.equals("")) {
            response.sendRedirect("/");
        }
        Integer raty = Integer.parseInt(request.getParameter("raty"));
        if (raty == null || raty.equals("")) {
            response.sendRedirect("/");
        }
        Float procent = Float.parseFloat(request.getParameter("procent"));
        if (procent == null || procent.equals("")) {
            response.sendRedirect("/");
        }
        Float oplataStala = Float.parseFloat(request.getParameter("oplataStala"));
        if (oplataStala == null || oplataStala.equals("")) {
            response.sendRedirect("/");
        }
        int oplata = Integer.parseInt(request.getParameter("oplata"));
        double q=0;
        double kapital;
        double odsetki = 0;
        double ckr;
        
        response.setContentType("text/html");
        for (int i = 1; i < raty+1; i++){
        q += (1 / Math.pow((1 + procent), i));
        if (oplata == 1){
            odsetki = (koszt - odsetki) * 0.035;
            kapital = (koszt/raty) - odsetki;
            ckr = kapital + odsetki;
        } else {
            odsetki = (koszt - odsetki) * 0.035;
            ckr = koszt + odsetki;
            kapital = koszt/raty;
        }
        response.getWriter().println("<head><style>div{"
                + "        border:1px solid black;"
                + "    }</style></head>"
                + "<body><div>Nr raty:" + i + "</div></body>"
                + "<div>Kwota kapita�u:" + kapital + "</div></body>"
                + "<div>Kwota odsetek:" + odsetki + "</div></body>"
                + "<div>Op�aty sta�e:" + oplataStala + "</div></body>"
                + "<div>Ca�kowita kwota raty:" + ckr + "</div><br />");
        }
        response.getWriter().println("</body>");
    }
}
