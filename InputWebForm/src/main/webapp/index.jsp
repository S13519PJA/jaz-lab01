<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Po�yczka Loan</title>
</head>
<body>
<form action="loan" method="post">
    <label>Kwota kredytu:<input type="number" id="koszt" placeholder="Wnioskowana Kwota" name="koszt"/></label><br />
    <label>Ilosc rat:<input type="number" id="raty" placeholder="W miesiacach" name="raty"/></label><br />
    <label>Oprocentowanie:<input type="number" step="any" id="procent" name="procent"/></label><br />
    <label>Oplata stala:<input type="number" id="oplataStala" readonly name="oplataStala"/></label><br />
    <label>Rodzaj oplaty:<select id="oplata" name="oplata">
        <option value="1">Stala</option>
        <option value="2">Malejaca</option>
    </select></label><br />
    <input type="submit" value="wyslij"/>
</form>
<script>
    $(" input ").change(function () {
        var koszt = $("#koszt").val();
        var raty = $("#raty").val();
        var procent = $("#procent").val();
        document.getElementById('oplataStala').value = (koszt *(procent / 100)) / raty;
    });
</script>
</body>
</html>
